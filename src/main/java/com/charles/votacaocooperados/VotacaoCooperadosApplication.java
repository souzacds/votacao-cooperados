package com.charles.votacaocooperados;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VotacaoCooperadosApplication {

	public static void main(String[] args) {
		SpringApplication.run(VotacaoCooperadosApplication.class, args);
	}

}
